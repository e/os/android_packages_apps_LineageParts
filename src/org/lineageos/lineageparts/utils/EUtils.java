/*
 * Copyright (C) 2023 MURENA SAS
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.lineageos.lineageparts.utils;

import android.content.Context;
import android.os.UserHandle;
import android.provider.Settings;

import java.util.UUID;


public class EUtils {

    public static void loadSentryID(Context context) {
        String sentryId = Settings.Secure.getStringForUser(
                context.getContentResolver(), Settings.Secure.SENTRY_USERID,
                UserHandle.USER_CURRENT);
        if (sentryId == null) {
            UUID uuid = UUID.randomUUID();
            Settings.Secure.putStringForUser(context.getContentResolver(),
                    Settings.Secure.SENTRY_USERID, uuid.toString(),
                    UserHandle.USER_CURRENT);
         }
    }
}
